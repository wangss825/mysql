 select * from A3Employee;
select e.empfirstname, e.emplastname, p.prodPrice* ol.qty *s.suppdiscount from A3Employee e 
        Join A3Order o on e.empno = o.empno join A3orderline ol on ol.ordno = o.ordno 
        join A3Product p on p.prodno = ol.prodno join A3Supplier s on s.suppNo = p.suppNo;   
        
select e.empfirstname, e.emplastname,sum(ol.qty),  sum(p.prodPrice* ol.qty  * e.empCommrate)  from A3Employee e 
Join A3Order o on e.empno = o.empno join A3orderline ol on ol.ordno = o.ordno 
join A3Product p on p.prodno = ol.prodno group by e.empfirstname, e.emplastname;

--question1
declare
    efname A3Employee.empfirstname%type;
    elname A3Employee.emplastname%type;
    cursor emp_cur is select e.empfirstname, e.emplastname,sum(ol.qty) as saleAmount,  sum(p.prodPrice* ol.qty  * e.empCommrate) as Amount  from A3Employee e 
    Join A3Order o on e.empno = o.empno join A3orderline ol on ol.ordno = o.ordno 
    join A3Product p on p.prodno = ol.prodno group by e.empfirstname, e.emplastname;
       
    Max1 number:=0;
    firstname1 A3Employee.empfirstname%type;
    lastname1  A3Employee.emplastname%type;
    qty1 A3Orderline.qty%type;
     
    Max2 number:=0;
    firstname2 A3Employee.empfirstname%type;
    lastname2  A3Employee.emplastname%type;
    qty2 A3Orderline.qty%type;
    
    Max3 number:=0;
    firstname3 A3Employee.empfirstname%type;
    lastname3  A3Employee.emplastname%type;
    qty3 A3Orderline.qty%type;
begin
    FOR emp_rec in emp_cur loop
        if emp_rec.amount > Max1 then       
        Max1 := emp_rec.amount;
        firstname1 :=emp_rec.empfirstname;     
        lastname1 :=emp_rec.emplastname;  
        qty1 :=emp_rec.saleAmount;
        end if;  
     end loop;
         
    FOR emp_rec in emp_cur loop
      if emp_rec.amount > Max2 and emp_rec.amount <> Max1 then
        Max2 := emp_rec.amount;
        firstname2 :=emp_rec.empfirstname;     
        lastname2 :=emp_rec.emplastname;  
        qty2 :=emp_rec.saleAmount;
      end if; 
    end loop;
          
    FOR emp_rec in emp_cur loop
      if emp_rec.amount > Max3 and emp_rec.amount <> Max1 and emp_rec.amount <> Max2 then
        Max3 := emp_rec.amount;
        firstname3 :=emp_rec.empfirstname;     
        lastname3 :=emp_rec.emplastname;  
        qty3 :=emp_rec.saleAmount;
      end if; 
    end loop;
    
    dbms_output.put_line(' Name:' || firstname1 || ' ' ||lastname1|| ',' || 'The totle amount of money is: ' || Max1 ||', The total number of items are: '|| qty1);
    dbms_output.put_line(' Name:' || firstname2 || ' ' ||lastname2|| ',' || 'The totle amount of money is: '|| Max2 || ', The total number of items are: '|| qty2);
    dbms_output.put_line(' Name:' || firstname3 || ' ' ||lastname3|| ',' || 'The totle amount of money is: ' || Max3 ||', The total number of items are: '|| qty3);         
end;

--question2
desc a3product;
desc a3OrderLine;

--select prodqoh , qty, A3product.prodNo from a3product join a3OrderLine on a3product.prodNo = a3OrderLine.prodNo;

select prodqoh , A3product.prodNo from a3product; 
--select sum(qty), A3product.prodNo from a3product join a3OrderLine on a3product.prodNo = a3OrderLine.prodNo group by A3product.prodNo;
 
create or replace function A3OrderCheck(ProdNo_fun in number, qty_fun in number) return boolean
is 
    cursor stock_cur is select prodqoh , A3product.prodNo from a3product;     
begin
    for stock_rec in stock_cur loop
        if ProdNo_fun=stock_rec.prodno then
             if stock_rec.prodqoh > qty_fun then
                 return true;
             elsif stock_rec.prodqoh < qty_fun then
                return false;
--             else
--                 return null;             
             end if;
        else
            return null;
        end if;  
        
        
    end loop;
end;

CREATE OR REPLACE FUNCTION BooleanToVarchar
  (bValue BOOLEAN)
  RETURN VARCHAR2
IS
  vResult VARCHAR2(5);
BEGIN
  IF bValue THEN
    vResult := 'true';
  ELSIF NOT bValue THEN
    vResult := 'false';
  ELSE
    vResult := 'null';
  END IF;
  RETURN vResult;
END;
    
begin
  dbms_output.put_line(BooleanToVarchar(A3OrderCheck(1066,666)));
end;

--question3
create or replace procedure A3UpdateQOH(prodNo_in in number, adjAmount in number) is
    vnumber A3Product.prodqoh%type :=0;
    invalid_quantity_adjustment exception;
    invalid_product_number exception;
    
begin
    select prodqoh into vNumber from a3product where prodNo_in = prodNo;
    if vNumber=0 then 
        RAISE invalid_product_number;
    elsif   vNumber - adjAmount <0 then
        RAISE invalid_quantity_adjustment;
    elsif adjAmount > 0 then 
        update a3product set prodQOh = prodQOH + adjAmount where prodNo = prodNo_in;
    elsif adjAmount < 0 then 
         update a3product set prodQOh = prodQOH - adjAmount where prodNo = prodNo_in; 
    end if;       
exception
    when NO_DATA_FOUND Then
    dbms_output.put_line('Invalid product number : '|| prodNo_in);
    raise_application_error(20301 , 'Invalid product number : '|| prodNo_in);
    when invalid_quantity_adjustment Then
    dbms_output.put_line('Invalid quantity adjustment:' || adjAmount);
    raise_application_error(-20302, 'Invalid quantity adjustment:' || adjAmount);
         
end;
begin
   A3UpdateQOH(1001, 77);
end;

select * from  a3product;

 


