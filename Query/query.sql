select * from hr.regions;
select * from hr.countries;
select * from hr.locations;
select * from hr.departments;
select * from hr.jobs;
select * from hr.employees;
select * from hr.job_history;

--1)	For any employees with a missing department id, list the employee's id, first name, and last name, and their manager's id,
--first name, last name, and department id.[4 marks]
SELECT E.employee_id,E.first_name, E.last_name, M.manager_id, M.first_name, M.last_name, M.department_id 
    FROM hr.employees E JOIN hr.employees M ON E.manager_id= M.employee_id AND E.department_id IS NULL; 
desc employees;


--2)	List the employee id, first name, last name, hire date (in the form of "November 16, 2009") and current job title for the employees who have the earliest hire date. [4 marks]
SELECT E.employee_id, E.first_name, E.last_name, TO_CHAR(E.hire_date,'Month dd, yyyy') AS "HIRE_DATE", J.job_title FROM employees E JOIN jobs J ON E.job_id = J.job_id AND E.hire_date = 
(SELECT MIN(hire_date) FROM employees);


--3)	List all locations (location id and city) and the departments associated with each location (department id and name),
--and the number of employees working for that department. All locations should be listed 
--even if there are no departments associated with that location. Order the results by location id.
--For locations with many departments, order by department id. [5 marks]
select * from locations;
select * from departments;
select * from employees;

SELECT COUNT(*) AS employee_number, L.location_id, L.city, D.department_name, D.department_id
      FROM locations L LEFT JOIN departments D ON L.location_id = D.location_id
      JOIN employees E ON D.department_id = E.department_id GROUP BY L.location_id, L.city, D.department_name, D.department_id ; 
           
select * from regions;
select * from countries;
select * from locations;
select * from departments;
select * from jobs;
select * from employees; 
select * from job_history;
desc job_history;
--4)	An employee is hired into an initial position and may change jobs while with the company. 
--The Employees table shows the current position. Old positions are tracked in the Job_History table. 
--Create a query that will list all employees (id, last name, first name) and the job id and title for all their jobs both current and previous.
--For previous jobs, show the end date, and for current positions display "present". Sort the results by id and then end date.[5 marks]
SELECT E.employee_id, E.last_name, E.first_name, J.job_id, J.job_title, 'present' AS end_date FROM employees E JOIN jobs J ON E.job_id =  J.job_id
UNION
SELECT E.employee_id, E.last_name, E.first_name, JH.job_id, J.job_title, TO_CHAR(JH.end_date) FROM employees E JOIN job_history JH ON E.employee_id =  JH.employee_id JOIN jobs J ON J.job_id =JH.job_id
ORDER BY employee_id, end_date;


select * from regions;
select * from countries;
select * from locations;
select * from departments;
select * from jobs;
select * from employees; 
select * from job_history;
desc job_history;
--5)Use a correlated subquery for this question.
--List departments (department id and name) and the average salary for the employees in each department 
--where the average salary for that department is more than the average salary for departments in the same location.[6 marks]
--average salary for each department
select sum(e.salary) as totle_salary, count(*) as totle_people, sum(e.salary)/count(*) as average_salary, e.department_id 
from employees e group by e.department_id;

--average salary for each location
select sum(e.salary) as totle_salary, count(*) as totle_people, sum(e.salary)/count(*) as average_salary, d.location_id  
from employees e join departments d on d.department_id = e.department_id  group by d.location_id ;
having d.location_id =1700;
--show the location_id
select dout.department_id, dout.department_name, dout.location_id, sum(e.salary)/count(*) as average_salary 
from departments dout join employees e on dout.department_id = e.department_id  group by dout.department_id, dout.department_name,dout.location_id 
having sum(e.salary)/count(*) > (select sum(e.salary)/count(*) from employees e join departments din on din.department_id = e.department_id  
group by din.location_id having dout.location_id =din.location_id);
-- don't show the location_id
SELECT DOUT.department_id, DOUT.department_name,SUM(E.salary)/COUNT(*) AS average_salary 
FROM departments DOUT JOIN employees E ON DOUT.department_id = E.department_id  GROUP BY DOUT.department_id, DOUT.department_name,DOUT.location_id 
HAVING SUM(E.salary)/COUNT(*) > (SELECT SUM(E.salary)/COUNT(*) FROM employees E JOIN departments DIN ON DIN.department_id = E.department_id  
GROUP BY DIN.location_id HAVING DOUT.location_id =DIN.location_id);



 

 